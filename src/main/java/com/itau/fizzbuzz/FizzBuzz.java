package com.itau.fizzbuzz;

import java.util.ArrayList;
import java.util.List;

public class FizzBuzz {

	public static String fizzBuzz(int i) {
		
		if ((i % 3 ==0) && (i % 5 ==0))  {
			return "fizzbuzz";
		}
		
		if (i % 5 == 0) {
			return "buzz";
		}
		
		if ( i % 3 == 0) {
			return "fizz";
			
		}

		return Integer.toString(i);				
	}
	
	
	public static List<String> LoopFizzBuzz(int i) {
		
		List lista = new ArrayList<>();
		
		for (int j = 1; j <= i; j++) {
			lista.add(fizzBuzz(j));
		}
		
		return lista;
	}

	
}

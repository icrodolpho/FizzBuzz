package com.itau.fizzbuzz;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest {

	@Test
	public void testaFizzBuzz3() {
		String ret = FizzBuzz.fizzBuzz(3);
		assertEquals(ret, "fizz");
	}

	@Test
	public void testaFizzBuzz5() {
		String ret = FizzBuzz.fizzBuzz(5);
		assertEquals(ret, "buzz");
	}

	@Test
	public void testaFizzBuzz() {
		String ret = FizzBuzz.fizzBuzz(3);
		assertEquals(ret, "fizz");
	}

	@Test
	public void testaFizzBuzz15() {
		String ret = FizzBuzz.fizzBuzz(15);
		assertEquals(ret, "fizzbuzz");
	}

	@Test
	public void testaFizzBuzzmulti3() {
		String ret = FizzBuzz.fizzBuzz(6);
		assertEquals(ret, "fizz");
	}

	@Test
	public void testaFizzBuzzmulti5() {
		String ret = FizzBuzz.fizzBuzz(10);
		assertEquals(ret, "buzz");
	}

	@Test
	public void testaFizzBuzzmulti15() {
		String ret = FizzBuzz.fizzBuzz(30);
		assertEquals(ret, "fizzbuzz");
	}

	@Test
	public void testaFizzBuzzNada() {
		String ret = FizzBuzz.fizzBuzz(17);
		assertEquals(ret, "17");
	}
	
	@Test
	public void testaLoopFizzBuzz() {
		List<String> ret = FizzBuzz.LoopFizzBuzz(15);
		assertEquals(ret.size(), 15);
	}

}
